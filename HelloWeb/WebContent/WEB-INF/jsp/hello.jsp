<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-language" content="en">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<spring:url value="/resources/hello.css" var="helloCSS" />
<link href="${helloCSS}" rel="stylesheet" />
</head> 

<body>

  
  
<div id="header">
<h1><a>Cotton Caress</a></h1>
<h3>${loggedInUser}</h3>
<ul id="menu">
  <li><a href="C:\Users\shashi!\Desktop\html\login.html">Customize</a></li>
  <li><a href="C:\Users\shashi!\Desktop\html\login.html">Cotton Caress Exclusive</a></li>
  <li><a href="C:\Users\shashi!\Desktop\html\login.html">Fabric</a></li>
  <li><a href="C:\Users\shashi!\Desktop\html\login.html">How It Works</a></li>
  <li><a href="C:\Users\shashi!\Desktop\html\login.html">About Us</a></li>
  <li><a href="C:\Users\shashi!\Desktop\html\login.html">Help</a></li>
  <li><a href="http://localhost:8081/HelloWeb/login">Login</a></li>
  <li><a href="C:\Users\shashi!\Desktop\html\sunny.gif">Cart</a></li>
</ul> 


</div>
<div id="section">
<h2 align="center">Bespoke
<img src="resources/bespoke.jpg" alt="Bespoke" style="float:right;width:450px;height:500px;">
</h2>

<p> <font face="verdana">
Every Cotton Caress Bespoke garment has its own individually handmade pattern, cut by a Master Cutter. 
It is then made under his or her personal supervision to the exacting standards for which Cotton Caress is famed.</font></p>
<br />

<p> <font face="verdana">
The Cotton Caress Bespoke Association is dedicated to protecting and promoting the practices and traditions that have made 
Cotton Caress the acknowledged home of the best bespoke tailoring and a byword for unequaled quality around the world.</font></p>


</div>

<div id="footer">
Copyright �  cottoncaress.com
</div>

</body>
</html>
