<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-language" content="en">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<spring:url value="/resources/login.css" var="loginCSS" />
<link href="${loginCSS}" rel="stylesheet" />
</head>
<body>
<h1>Log in</h1>
<h2>${message}</h2>

			<div id="newcustomer">
				<div class="fancyContentBox">
					<h3>Are you a new customer?</h3>
					<a href="/HelloWeb/register" class="infoButton" title="Click here to create your account">Create account</a>
				</div>
			</div>
			<div id="login">
			<div class="fancyContentBox">
				<h3>Are you an existing customer?</h3>
				<form class="row fancyForm" action="/HelloWeb/loginsucess" method="post">
					<div class="c-24">
						<p>Please enter your e-mail address and password below to log in.</p>
					</div>
					<div class="c-12 c-m-24 ff-input ff_email">	<label for="email">E-mail address<span class="required">&bull;</span></label>
	<input  type="email" name="email" id="email" class="formfield long" />
</div><div class="c-12 c-m-24 ff-input ff_password">	<label for="password">Password<span class="required">&bull;</span></label>
	<input  type="password" name="password" id="password" class="formfield small" />
</div>					<div class="buttongrp c-24">
							<input  type="submit" value="Log in" class="infoButton m1t fullwidthmobile"/>
					</div>
					<div class="c-24">
						<br />
						<a href="/forgotpass">Click here if you have forgotten your password</a>
					</div>
				</form>
			</div>
			</div>
			
			<div class="hidden">
				<div id="newAccountModal">
					<div class="center">
						<p class="message"></p>
						<button class="infoButton small createNewAccountButton">Yes</button>
						<button class="discreteButton small linkAccountButton">No</button>
					</div>
				</div>
			</div>		
	
	</body>
</html>